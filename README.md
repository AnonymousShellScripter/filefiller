# filefiller

Usage: `filefiller file blocks`

fills a file __file__ with __blocks__ blocks of size __BLOCKSIZE__

- if __TUI__ is defined, print statistics (progressbar size is defined in __BARLEN__)
- if __DEBUG__ is defined, print debug stuff

## TODO

- clean up code
- add comments

## DONE

- add transfer speed indicator
