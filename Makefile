SHELL = /bin/sh
.SUFFIXES = .c

CFLAGS = -Wall -Wextra --std=c99 -Ofast -flto -fPIC -march=native

CC=gcc
STRIP=strip

default:
	$(CC) $(CFLAGS) filefiller.c -o filefiller
	$(STRIP) ./filefiller

install:
	cp filefiller /usr/bin/filefiller

clean:
	rm filefiller
